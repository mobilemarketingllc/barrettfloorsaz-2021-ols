<?php

function promotion_func(){

    ?>
    <style>
        tr.salerow {border: 1px solid;}
    </style>
    <div class="wrap" id="grandchild-backend">
            <h2>Retailer Promotions </h2>

    <div id="wpcontent1" class="client_info_wrap">
             <form name="pluginname" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>Settings</h4></th>
                </tr>
                
                
            <tr>
            <td width="150px">
                <label for="clientsecret">Sale information Json <strong>*</strong></label>
            </td>
            <td class="form-group">
                <textarea name="saleinformation" style="width:70%;"><?php echo get_option('saleinformation')?></textarea>

            </td>
        </tr>

        
        <tr>
            <td width="150px">
                <label for="clientsecret">Overwrite Module <strong>*</strong></label>
            </td>
            <td class="form-group">
                <input type="checkbox" name="overright_module" value="1" />Overright Modules(Content Slider, post grid etc.)

            </td>
        </tr>
        <tr>
            <td></td>
            <td class="form-group">
                <button id="syncdata" type="submit" class="button button-primary   button-hero" >Save</button>
            </td>
        </tr>
                
        <tr>
            <th colspan="2"><h4>Current Active Promotions</h4></th>
        </tr>                
        
        <?php $saleinfo = get_option('salesliderinformation');

        $saleinfodd = get_option('promos_json');

        $saleinfodd = json_decode($saleinfodd, True);        

        write_log($saleinfodd);

        if($saleinfo != ''){
        
        $saleinfo = json_decode($saleinfo, True);        

       // write_log($saleinfo);

        foreach($saleinfodd as $sale){

        ?>
        <tr class="salerow">
            <td width="150px">
                <span><b><?php echo $sale['name']; ?> - <?php echo $sale['promoCode'];  ?></b></span>
            </td>
            <td width="150px">
                <span><?php echo 'Start Date -: '.$sale['startDate'];  ?> - <?php echo 'End Date -: '.$sale['endDate'];  ?> </span>
                <br>
                <span><?php echo 'Priority -: '.$sale['priority'];  ?> </span>
                <br>
                <span><?php echo 'getCoupon -: '.$sale['getCoupon'];  ?> </span>
                <br>                
                <?php 
                
                foreach($sale['widgets'] as $widget){
                    ?>

                        <br>  
                        <span> <?php echo 'Widget Name -: '.$widget['name'];  ?></span><br>                       
                        <span><?php echo 'Type -: '.$widget['type'];  ?> </span>
                        <?php if($widget['type'] == 'slide'){   ?>                  
                        
                        <p>  <?php echo 'Slide Order -: '.$widget['order'];  ?></p>
                        <p> <?php echo 'Slide Link -: '.$widget['link'];  ?></p>                       
                        
                        <?php } ?>
                        <?php if($widget['type'] == 'landing'){ if($widget['texts'][0]['text']!=''){$copy = 'Assigned';}else{$copy = 'Not Assigned';}
                        if($widget['texts'][1]['text']!=''){$copyh = 'Assigned';}else{$copyh = 'Not Assigned';}
                        ?>                  
                        
                        <p>  <?php echo 'Coupon copy -: '.$copy ;  ?></p>
                        <p> <?php echo 'Heading -: '.$copyh;  ?></p>                       
                        
                        <?php } ?>
                        <?php if($widget['type'] == 'banner'){   ?>    
                        
                        <p> <?php echo 'Banner Link -: '.$widget['link'];  ?></p>                       
                        
                        <?php } ?>
                        <br>
                        
              <?php  }
                ?>
            </td>
        </tr>     
       
        <?php } }?>   
        </table>    
        </form>
        </div>
        </div>
    <?php
}